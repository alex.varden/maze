function Player(maze){

    const DIR_LEFT  = 'left';
    const DIR_RIGHT = 'right';
    const DIR_UP    = 'up';
    const DIR_DOWN  = 'down';

    this.maze = maze;
    this.cell = this.maze.grid[0];
    this.colour = '#E1045C';

    this.draw = function(){
        this.cell.highlight(0.5,this.colour);
    }

    this.foundPrize = function(){
      if(this.cell.prize){
        this.cell.prize.payload();
      }
    }


    this.move = function(dir){


        var next = false;

        switch (dir) {
            case DIR_LEFT:
                next = this.maze.getCell(this.cell.i - 1, this.cell.j ); //left
            break;

            case DIR_RIGHT:
                next = this.maze.getCell(this.cell.i + 1, this.cell.j ); //right
            break;

            case DIR_UP:
                next = this.maze.getCell(this.cell.i, this.cell.j - 1); //top
            break;

            case DIR_DOWN:
                next = this.maze.getCell(this.cell.i, this.cell.j + 1); //bottom
            break;
        }

        if (next && typeof next != "undefined" ) {
            if(this.maze.canTravelTo(this.cell,next)){
                this.cell = next;
                this.foundPrize();

            }

        }

    }



}
