    function Cell(i, j, maze) {

    this.i = i;
    this.j = j;
    this.maze = maze;

    this.size = this.maze.blockSize;


    this.visited = false;
    this.current = false;
    this.walls = [true, true, true, true];

    this.highlight = function(figureSize,colour){

        var size  = this.size ;
        figureSize = size * figureSize;


        var x = this.i * size;
        var y = this.j * size;

        noStroke();
        fill(colour);
        ellipse(x+(size/2) , y+(size/2) , figureSize, figureSize);

    }


    this.checkNeighbors = function (grid) {
        var neighbors = [];

        var top    = this.maze.getCell(i,     j - 1); //top
        var right  = this.maze.getCell(i + 1, j    ); //right
        var bottom = this.maze.getCell(i,     j + 1); //bottom
        var left   = this.maze.getCell(i - 1, j    ); //left

        if(top && !top.visited){
            neighbors.push(top);
        }

        if(right &&!right.visited){
            neighbors.push(right);
        }

        if(bottom &&!bottom.visited){
            neighbors.push(bottom);
        }

        if(left &&!left.visited){
            neighbors.push(left);
        }

        return this.pickNext(neighbors);

    };

    this.pickNext= function(neighbors){

        if(neighbors.length > 0){
            var r =  floor(random(0,neighbors.length));
            return neighbors[r]; // random neighbor
        }else{
            return undefined;
        }
    }



    this.show = function () {

        var w = this.size;
        var x = this.i * this.size;
        var y = this.j * this.size;

        strokeWeight(2); // Beastly
        stroke('255');

        if(this.walls[0]){
            line(x, y, x + w, y); // top
        }
        if(this.walls[1]){
            line(x + w, y, x + w, y + w); //right
        }
        if(this.walls[2]){
            line(x, y + w, x + w, y + w); //bottom
        }
        if(this.walls[3]){
            line(x, y, x, y + w); // left
        }

    }

}
