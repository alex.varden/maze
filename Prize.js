function Prize(maze,gridRef){

    this.maze = maze;
    this.cell = this.maze.grid[gridRef];
    this.colour = '#efda1f';
    this.cell.prize = this;
    this.points = 10;



    this.draw = function(){
        this.cell.highlight(0.5,this.colour);
    }

    this.payload = function(){
      updatePoints(this.points);
    }

    
}
