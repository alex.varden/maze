
var currentMaze  ;
var player;
var speed = 1000 ;

var points = 0;


function setup() {
  currentMaze = new Maze();
  player  = new Player(currentMaze);
  currentMaze.generate();
}

function draw() {

    frameRate(speed);

    createCanvas(currentMaze.canvasSize,currentMaze.canvasSize);
    background(currentMaze.background);

    currentMaze.draw();
    currentMaze.drawPrizes();
    player.draw();
 }



  function updatePoints(add){
    points += add;

  }
