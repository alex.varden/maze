
function Maze() {
    this.cols;
    this.rows;
    this.blockSize = 100;
    this.grid = [];
    this.currentCell;
    this.stack = [];
    this.start = true;
    this.canvasSize = 500;
    this.background = '#282c34';
    this.continue = true;
    this.prizes = [];

    this.cols = floor(this.canvasSize / this.blockSize);
    this.rows = floor(this.canvasSize / this.blockSize);

    for (var j = 0; j < this.rows; j++) {
        for (var i = 0; i < this.cols; i++) {
            var cell = new Cell(i, j, this);
            this.grid.push(cell);
        }
    }

    this.currentCell = this.grid[0];
    this.player = this.grid[0];

    this.generateSize =  function(size){
        // this.canvasSize = 900;
        // this.blockSize = size;
        this.generate();
    }

    this.generate =  function(){
        while(this.continue){
            this.next();
        }
    }

    this.generatePrize = function(cell){
       this.prizes.push(new Prize(this,this.index(cell.i,cell.j)));
    }
    this.generateRandomPrize = function(cell){
       this.generatePrize
    }

    this.draw =  function(){
        for (var i = 0; i < this.grid.length; i++) {
            this.grid[i].show();
        }

    }

    this.drawPrizes = function (){
        for (var i = 0; i < this.prizes.length; i++) {
            this.prizes[i].draw();
        }
    }



    this.next =  function(){

        this.currentCell.visited = true;

        // Step 1 ====  Choose randomly one of the unvisited neighbours
        var nextCell = this.currentCell.checkNeighbors(this.grid);

        if(nextCell){
            nextCell.visited = true;

            // Step 2 ==== Push this.currentCell Cell to this.stack
            this.stack.push(this.currentCell);

            // Step 3 ==== Remove the wall between the this.currentCell cell and the chosen cell
            this.removeWalls(this.currentCell , nextCell);

            // STEP 4
            this.currentCell = nextCell;


        } else if (this.stack.length > 0){

            this.generatePrize(this.currentCell);

            this.currentCell = this.stack.pop();
        }else{
            this.continue = false;
        }

    }

    this.getCell = function(i,j){
        return this.grid[this.index(i,j)];
    }

    this.index = function(i, j) {
        if(i < 0 || j < 0 ||  i > this.cols - 1 || j > this.rows - 1){
            return -1;
        }
        return i + j * this.cols;
    }

    this.removeWalls = function(a,b){
        var x = a.i - b.i;

        if(x === 1){
            a.walls[3] = false;
            b.walls[1] = false;
        }else if( x === -1 ){
            a.walls[1] = false;
            b.walls[3] = false;
        }

        var y = a.j - b.j;

        if(y === 1){
            a.walls[0] = false;
            b.walls[2] = false;
        }else if( y === -1 ){
            a.walls[2] = false;
            b.walls[0] = false;
        }
    }


    this.canTravelTo = function(a,b){

       var x = a.i - b.i;

       if(x === 1){
           if(a.walls[3]  || b.walls[1] ){
               return false;
           }

        }else if( x === -1 ){
            if(b.walls[3]  || a.walls[1] ){
                return false;
            }
        }

        var y = a.j - b.j;

        if(y === 1){
            if(b.walls[2]  ||  a.walls[0] ){
                return false;
            }
        }else if( y === -1 ){
            if(a.walls[2]  || b.walls[0] ){
                return false;
            }
        }
        return true;
    }

    return this;

}
